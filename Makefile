
.PHONY: check test
check:
	@vendor/bin/phpstan analyse --no-progress

test:
	@vendor/bin/phpunit test
