<?php

namespace Test\Service;

use PaymentBundle\Payment\PaymentHandlerInterface;
use PaymentBundle\Payment\PaymentInterface;
use PaymentBundle\Payment\PaymentProviderInterface;
use PaymentBundle\Service\YandexKassa;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use YooKassa\Client;
use YooKassa\Request\Payments\PaymentResponse;

/** @covers \PaymentBundle\Service\YandexKassa */
class YandexKassaTest extends TestCase
{
    private static function getSuccessData(): string
    {
        return <<<JSON
{
    "type":"notification",
    "event":"payment.succeeded",
    "object":{
        "id":"28024772-000f-5000-9000-1f0bb8eec916",
        "status":"succeeded",
        "paid":true,
        "amount":{
            "value":"7999.00",
            "currency":"RUB"
            },
            "created_at":"2021-04-09T11:00:02.563Z",
            "description":"Payment for promotion from user",
            "metadata":[],
            "recipient":{
                "account_id":"111",
                "gateway_id":"222"
            },
            "refundable":false,
            "test":false,
            "cancellation_details":{
            "party":"yandex_checkout",
            "reason":"expired_on_confirmation"
        }
    }
}
JSON;
    }

    private static function getCancelData(): string
    {
        return <<<JSON
{
    "type":"notification",
    "event":"payment.canceled",
    "object":{
        "id":"28024772-000f-5000-9000-1f0bb8eec916",
        "status":"canceled",
        "paid":false,
        "amount":{
            "value":"7999.00",
            "currency":"RUB"
            },
            "created_at":"2021-04-09T11:00:02.563Z",
            "description":"Payment for promotion from user",
            "metadata":[],
            "recipient":{
                "account_id":"111",
                "gateway_id":"222"
            },
            "refundable":false,
            "test":false,
            "cancellation_details":{
            "party":"yandex_checkout",
            "reason":"expired_on_confirmation"
        }
    }
}
JSON;
    }

    public function getTestUpdateData()
    {
        return [
            ['successPayment', self::getSuccessData()],
            ['failedPayment', self::getCancelData()],
        ];
    }

    /**
     * @dataProvider getTestUpdateData
     * @covers \PaymentBundle\Service\YandexKassa::updatePayment
     */
    public function testUpdatePayment($method, $notification)
    {
        $json = json_decode($notification, true);
        $paymentResponse = new PaymentResponse($json['object']);

        $payment = new class implements PaymentInterface {
            public function getPaymentId(): string
            {
                return '28024772-000f-5000-9000-1f0bb8eec916';
            }
        };

        $paymentProvider = $this->createMock(PaymentProviderInterface::class);
        $paymentProvider->method('getByPaymentId')
            ->willReturn($payment);

        $paymentHandler = $this->createMock(PaymentHandlerInterface::class);
        $paymentHandler->expects($this->once())
            ->method($method)
            ->withConsecutive([$this->equalTo($payment)]);

        $client = $this->createMock(Client::class);
        $client->method('getPaymentInfo')
            ->willReturn($paymentResponse);

        $kassa = new YandexKassa('login', 'password', false, $paymentProvider, $paymentHandler, new NullLogger(),  $client);

        $kassa->handleNotification($json);
    }

    public function getTestIgnoreCanceledData()
    {
        return [
            [self::getCancelData(), false, 1],
            [self::getCancelData(), true, 0],
        ];
    }

    /**
     * @dataProvider getTestIgnoreCanceledData
     */
    public function testIgnoreCanceled(string $data, $ignoreCanceled, $callHandler)
    {
        $json = json_decode($data, true);
        $paymentResponse = new PaymentResponse($json['object']);

        $payment = new class implements PaymentInterface {
            public function getPaymentId(): string
            {
                return '28024772-000f-5000-9000-1f0bb8eec916';
            }
        };

        $paymentProvider = $this->createMock(PaymentProviderInterface::class);
        $paymentProvider->method('getByPaymentId')
            ->willReturn($payment);

        $paymentHandler = $this->createMock(PaymentHandlerInterface::class);
        $paymentHandler->expects($this->exactly($callHandler))
            ->method('failedPayment')
            ->withConsecutive([$this->equalTo($payment)]);

        $client = $this->createMock(Client::class);
        $client->method('getPaymentInfo')
            ->willReturn($paymentResponse);

        $kassa = new YandexKassa('login', 'password', $ignoreCanceled, $paymentProvider, $paymentHandler, new NullLogger(), $client);

        $kassa->handleNotification($json);
    }
}
