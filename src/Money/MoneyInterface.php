<?php

namespace PaymentBundle\Money;

interface MoneyInterface
{
    public function getAmount(): float ;

    public function getCurrency(): string;
}