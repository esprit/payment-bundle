<?php

namespace PaymentBundle\DependencyInjection;

use PaymentBundle\Service\YandexKassa;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class PaymentExtension extends Extension
{
    /**
     * @param array<string, mixed> $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (! isset($config['yandex'])) {
            return;
        }

        $provider = $container->resolveEnvPlaceholders($config['provider']);
        if (!is_string($provider)) {
            throw new InvalidConfigurationException('Invalid provider');
        }
        $handler = $container->resolveEnvPlaceholders($config['handler']);
        if (!is_string($handler)) {
            throw new InvalidConfigurationException('Invalid handler');
        }
        $ignoreCanceled = $config['ignore_canceled'];

        $login = $container->resolveEnvPlaceholders($config['yandex']['login']);
        $password = $container->resolveEnvPlaceholders($config['yandex']['password']);

        $definition = $container->getDefinition(YandexKassa::class);
        $definition->setArgument(0, $login);
        $definition->setArgument(1, $password);
        $definition->setArgument(2, $ignoreCanceled);
        $definition->setArgument(3, new Reference($provider));
        $definition->setArgument(4, new Reference($handler));
    }
}
