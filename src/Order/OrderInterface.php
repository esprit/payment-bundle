<?php

namespace PaymentBundle\Order;

interface OrderInterface
{
    /** @return int|float */
    public function getPrice();
    public function getCurrency(): string;

    public function getEmail(): string;

    /**
     * @return OrderItemInterface[]
     */
    public function getItems(): array;
}
