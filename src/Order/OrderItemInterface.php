<?php

namespace PaymentBundle\Order;

interface OrderItemInterface
{
    /** @return int|float */
    public function getPrice();

    public function getQuantity(): int;

    public function getTitle(): string;
}