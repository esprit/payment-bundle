<?php

namespace PaymentBundle\Exception;

use LogicException;

class UnexpectedDataException extends LogicException
{

}