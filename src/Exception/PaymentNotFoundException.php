<?php

namespace PaymentBundle\Exception;

use LogicException;

class PaymentNotFoundException extends LogicException
{

}