<?php

namespace PaymentBundle\Payment;

use PaymentBundle\Exception\PaymentNotFoundException;

interface PaymentProviderInterface
{
    /**
     * @throws PaymentNotFoundException
     */
    public function getByPaymentId(string $id): PaymentInterface;
}