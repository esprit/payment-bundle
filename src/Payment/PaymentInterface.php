<?php

namespace PaymentBundle\Payment;

interface PaymentInterface
{
    public function getPaymentId(): string;
}