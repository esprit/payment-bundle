<?php

namespace PaymentBundle\Payment;

interface PaymentHandlerInterface
{
    public function successPayment(PaymentInterface $payment): void;

    public function failedPayment(PaymentInterface $payment): void;
}
