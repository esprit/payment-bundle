<?php

namespace PaymentBundle\Service;

use PaymentBundle\Exception\UnexpectedDataException;
use PaymentBundle\Money\MoneyInterface;
use PaymentBundle\Order\OrderInterface;
use PaymentBundle\Payment\PaymentHandlerInterface;
use PaymentBundle\Payment\PaymentInterface;
use PaymentBundle\Payment\PaymentProviderInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use YooKassa\Client;
use YooKassa\Model\Notification\AbstractNotification;
use YooKassa\Model\Notification\NotificationCanceled;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\NotificationEventType;
use YooKassa\Model\PaymentStatus;
use YooKassa\Request\Payments\CreatePaymentResponse;

class YandexKassa
{
    /** @var Client */
    private $client;

    /** @var PaymentProviderInterface */
    private $paymentProvider;

    /**
     * @var PaymentHandlerInterface
     */
    private $paymentHandler;

    private $ignoreCanceled;
    private $logger;

    public function __construct(string $login, string $password, bool $ignoreCanceled, 
                                PaymentProviderInterface $paymentProvider,
                                PaymentHandlerInterface $paymentHandler, 
                                LoggerInterface $logger,
                                Client $client = null)
    {
        $this->client = $client ?: new Client();
        $this->client->setAuth($login, $password);
        $this->paymentProvider = $paymentProvider;
        $this->paymentHandler = $paymentHandler;
        $this->ignoreCanceled = $ignoreCanceled;
        $this->logger = $logger;
    }

    public function createPayment(OrderInterface $order, string $description, string $returnUrl) : ?CreatePaymentResponse
    {
        $items = [];
        foreach ($order->getItems() as $item) {
            $items[] = [
                "description" => $item->getTitle(),
                "quantity" => $item->getQuantity(),
                "amount" => [
                    'value' => $item->getPrice(),
                    'currency' => $order->getCurrency(),
                ],
                "vat_code" => 1, // https://yookassa.ru/developers/54fz/parameters-values#vat-codes
            ];
        }

        return $this->client->createPayment(
            [
                'amount' => [
                    'value' => $order->getPrice(),
                    'currency' => $order->getCurrency(),
                ],
                'confirmation' => [
                    'type' => 'redirect',
                    'return_url' => $returnUrl,
                ],
                'capture' => true,
                'description' => $description,
                'receipt' => [
                    'customer' => [
                        'email' => $order->getEmail(),
                    ],
                    'items' => $items
                ]
            ]
        );
        // TODO add custom idempotenceKey? @see https://kassa.yandex.ru/docs/checkout-api/#sozdanie-platezha
    }

    /**
     * @param array<string, mixed> $data
     */
    public function handleNotification(array $data): void
    {
        /** @var NotificationSucceeded|NotificationCanceled $notification */
        $notification = self::getNotification($data);
        if ($this->ignoreCanceled and ($notification instanceof NotificationCanceled)) {
            $this->logger->info('Skip canceled notification');
            return;
        }
        $object = $notification->getObject();

        $id = $object->getId();
        $payment = $this->paymentProvider->getByPaymentId($id);

        $this->updatePayment($payment);
    }

    public function updatePayment(PaymentInterface $payment): void
    {
        $status = $this->getPaymentStatus($payment->getPaymentId());

        if (PaymentStatus::SUCCEEDED === $status) {
            $this->paymentHandler->successPayment($payment);

            return;
        }

        if (PaymentStatus::CANCELED === $status) {
            $this->paymentHandler->failedPayment($payment);

            return;
        }

        throw new UnexpectedDataException('Unexpected payment type');
    }

    private function getPaymentStatus(string $id): string
    {
        $info = $this->client->getPaymentInfo($id);
        if ($info === null) {
            throw new RuntimeException('Something wrong');
        }

        if ($info->getId() !== $id) {
            throw new RuntimeException('Received payment id does not matches with requested one');
        }

        return $info->getStatus();
    }

    /**
     * @param array<string, mixed> $data
     */
    private static function getNotification(array $data): AbstractNotification
    {
        if ($data['event'] === NotificationEventType::PAYMENT_SUCCEEDED) {
            return new NotificationSucceeded($data);
        }

        if ($data['event'] === NotificationEventType::PAYMENT_CANCELED) {
            return new NotificationCanceled($data);
        }

        throw new UnexpectedDataException('Unexpected notification type');
    }
}
