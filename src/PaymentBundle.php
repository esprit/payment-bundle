<?php

namespace PaymentBundle;

use PaymentBundle\DependencyInjection\PaymentExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PaymentBundle extends Bundle
{
    public function getContainerExtension(): PaymentExtension
    {
        return new PaymentExtension();
    }
}
