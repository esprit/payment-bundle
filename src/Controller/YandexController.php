<?php

namespace PaymentBundle\Controller;

use PaymentBundle\Exception\PaymentNotFoundException;
use PaymentBundle\Exception\UnexpectedDataException;
use PaymentBundle\Service\YandexKassa;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class YandexController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function notification(YandexKassa $yandexKassa, Request $request): Response
    {
        /** @var string $contest */
        $contest = $request->getContent();
        if (null === $data = json_decode($contest, true)) {
            throw new BadRequestHttpException('Empty data received');
        }

        if (!is_array($data)) {
            throw new BadRequestHttpException('Invalid data received');
        }

        $this->logger->debug('request data', $data);

        try {

            $yandexKassa->handleNotification($data);

        } catch (UnexpectedDataException|PaymentNotFoundException|PaymentNotFoundException $e) {
            $this->logger->error($e);
        }

        return new Response();
    }
}
